﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using OpenTerraria;
using OpenTerraria.Client;
using OpenTerraria.Model.ID;

namespace TerrariaBot
{
    internal static class Program
    {
        private static void Main()
        {
            dynamic config = JObject.Parse(File.ReadAllText("config.json"));
            if (config == null) throw new InvalidOperationException("Failed parsing config.json");

            TerrariaClient client = new TerrariaClient(cfg =>
            {
                cfg.Player(player =>
                {
                    player.Health(400);
                    player.Appearance(appear =>
                    {
                        appear.Name("Bot Client");
                    });
                    player.Inventory(inv =>
                    {
                        inv.AddAccessory(ItemId.PutridScent);

                        inv.SetHelmet(ItemId.DTownsHelmet);
                        inv.SetChestplate(ItemId.DTownsBreastplate);
                        inv.SetLeggings(ItemId.DTownsLeggings);
                    });
                });
            });

            Task.Run(async () =>
            {
                client.Log.MessageReceived += (s, e) => Console.WriteLine(e.Message);
                client.Services.Add<BasicService>();

                await client.ConnectAndLogin((string)config.Host, (int)config.Port);
            });

            client.Wait();

            Console.WriteLine("\r\nPress any key to continue...");
            Console.ReadLine();
        }
    }
}