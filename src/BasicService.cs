﻿using System;
using OpenTerraria.Client;
using OpenTerraria.Client.Service;
using OpenTerraria.Model;
using OpenTerraria.Packet;
using OpenTerraria.Commands;

namespace TerrariaBot
{
    public class BasicService : IService
    {
        private TerrariaClient _client;
        
        private AnglerQuest _currentQuest;

        public void Install(TerrariaClient client)
        {
            _client = client;
            
           _client.Services.Get<CommandService>()
           .Add("echo")
           .Parameter("msg", ParameterType.Unparsed)
           .Do(e => 
           {
               client.CurrentPlayer.SendMessage(e.GetArg("msg"));
           });
            
            PacketEventService packets = _client.Services.Get<PacketEventService>();

            packets.Subscribe(TerrPacketType.AnglerQuest, packet =>
            {
                AnglerQuest quest = PacketWrapper.Parse<AnglerQuest>(packet);
                
                if(_currentQuest == null || _currentQuest.Quest != quest.Quest)
                    client.CurrentPlayer.SendMessage($"New angler quest: ID: {quest.Quest}");
                
                _currentQuest = quest;
            });

            client.Connected += (s, e) => Console.WriteLine("Connected.");
            client.Disconnected += (s, e) => Console.WriteLine($"Disconnected: {e.Reason}");
            client.LoggedIn += (s, e) =>
            {
                Console.WriteLine("Logged in.");

                /* 
                   IMPORTANT : 
                   If your client is a command bot I recommend killing it on login.
                   This way it shouldn't interfere with monster ai and spawns.
                */

                _client.CurrentPlayer.Killme();
            };
            client.StatusReceived += (s, e) => Console.WriteLine($"Status: {e.Status.Text}");
            /*
            client.MessageReceived += (s, e) =>
            {
                Console.WriteLine($"<{client.GetExistingPlayer(e.Message.PlayerId).Appearance.Name}> {e.Message.Text}");

                if (e.Sender != MessageReceivedEventArgs.SenderType.Player) return;

                try
                {
                    // quick command hack thingy
                    string[] words = e.Message.Text.Split(' ');
                    switch (words[0])
                    {
                        case "!quest":
                            if(_currentQuest == null)
                            {
                                client.CurrentPlayer.SendMessage("No quest.");
                                return;                                
                            }

                            client.CurrentPlayer.SendMessage($"New angler quest: ID: {_currentQuest.Quest}");
                            break;
                    }
                }
                catch (Exception)
                {
                }
            };
            */
        }
    }
}